
# Создаём правило для балансировщика
resource "google_compute_global_forwarding_rule" "default" {
  name                  = "forwarding-rule-diplom1"
  ip_protocol           = "TCP"
  load_balancing_scheme = "EXTERNAL"
  port_range            = "80"
  target                = google_compute_target_http_proxy.default.id
  ip_address            = google_compute_global_address.default.id
}

# Создаём прокси для балансировщика
resource "google_compute_target_http_proxy" "default" {
  name     = "target-http-proxy-diplom1"
  url_map  = google_compute_url_map.default.id
}

# Создаём url-map для балансировщика
resource "google_compute_url_map" "default" {
  name            = "url-map-diplom1"
  default_service = google_compute_backend_service.default.id
}

# Создаём backend сервис для балансировщика
resource "google_compute_backend_service" "default" {
  name                     = "backend-service-diplom1"
  protocol                 = "HTTP"
  port_name                = "http"
  load_balancing_scheme    = "EXTERNAL"
  timeout_sec              = 10
  health_checks            = [google_compute_health_check.default.id]
  backend {
    group           = google_compute_instance_group_manager.default.instance_group
    balancing_mode  = "UTILIZATION"
    capacity_scaler = 1.0
  }
}

# Создаём health check
resource "google_compute_health_check" "default" {
  name     = "hc-diplom1"
  http_health_check {
    port_specification = "USE_SERVING_PORT"
  }
}
